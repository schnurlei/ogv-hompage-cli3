### Willkommen beim Obst- und Gartenbauverein Schießen e.V.

> _"..enrich everyone's life through plants and make the UK a greener, more beautiful place."_

Das ist das Motto der Royal Horticultural Society und es beschreibt auch passend das Motto unserer Gartenbauvereine:
**Unsere Dörfer und Städte grüner und lebenswerter zu machen und das Leben 
von allen Menschen durch Pflanzen und Gärten zu verschönern.**

Wir glauben, dass Blumen, Käuter, Bäume und alle anderen Pflanzen einen wichtigen Beitrag dazu leisten,
dass Menschen sich wohler und gesünder fühlen.

Es ist eine Freude, im Frühjahr einen blühenden Baum zu bestaunen, im Sommer sich an duftenden Blumen zu erfreuen 
und im Herbst sein selbst angebautes Gemüse zu genießen.

Als Obst- und Gartenbauverein wollen wir dazu unseren Beitrag leisten.
Wir freuen uns, wenn auch Sie dabei mithelfen würden.

