## 90 Bäume

Wenn wir schon nicht feiern können, so wollen wir diese stolze Zahl doch gebührend wür­digen. Gemeinsam mit Euch, den Bürgern unserer Gemeinde, möchten wir für jedes dieser Jahre einen Baum pflanzen.
So wollen wir unsere Gärten verschönern und für mehr Viel­falt in unserer Gemeinde sorgen.
Der OGV Schießen fördert jeden neu gepflanzten Baum mit **25 €** und die Gemeinde Roggenburg legt nochmal **10 €** drauf. 

**Seid Ihr dabei?**

