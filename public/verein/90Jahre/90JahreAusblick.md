## Wie geht es weiter

Die Aktion "90 Jahre - 90 Bäume" soll nur ein Startschuss sein für mehr Vielfalt in unserer Gemeinde. Die Gartenbauvereine Biberach/Asch und Schießen tragen mit vielen Aktionen dazu bei. Dies gelingt aber nur, wenn möglichst viele dabei mitmachen. 

### Hier ein paar Ideen, für mehr Vielfalt: 

Sagen Sie uns, wo Sie in unserer Gemeinde noch einen Baum vermissen.

Machen Sie Ihren Garten lebendiger, bunter und vielfältiger. Im Frühjahr stellen wir dazu kostenloses Saatgut für vielfältige Blumenwiesen zur Verfügung.

Wenn Sie keinen Platz für einen eigenen Obstbaum haben, werden Sie doch [Baumpate](#/themen/Streuobst).

Wenn Sie weitere Informationen zum Thema Vielfalt im Garten wünschen, fragen Sie uns einfach.  

Oder Informieren Sie sich über die Webseite unseres [Landesverbandes](https://www.gartenbauvereine.org/fachinformationen-3/)  
Oder über die [Vielfaltsmacher](https://www.vielfaltsmacher.de/) Homepage  
Oder über die [Bayerische Gartenakademie](https://www.lwg.bayern.de/gartenakademie/ratgeber/index.php)
