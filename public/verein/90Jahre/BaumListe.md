## Pflanzenliste
### Obstgehölze

Name | Botanischer Name 
-------- | -------- 
Kultur-Apfel | Malus domestica
Pfirsich | Prunus persica
Aprikose | Prunus armeniaca 'Kioto'
Sauer-Kirsche | Prunus cerasus
Pflaume/Zwetschge | Prunus domestica
Süß-Kirsche	| Prunus avium
Birne | Pyrus communis
Quitte | Cydonia oblonga
Walnuss | Juglans regia

### Ziergehölze

Name | Botanischer Name
-------- | -------- 
Zieräpfel |	Malus domestica, Malus floribunda
Gewöhnliche Stechpalme	| Ilex aquifolium
Eibe | Taxus baccata
Blumenesche |	Fraxinus ornus
 
### Wildgehölze
Name | Botanischer Name
-------- | -------- 
Haselnuß | Corylus avellana
Kornelkirsche |	Cornus mas
Sal-Weide, Palm-Weide |	Salix caprea, S. c. ' Mas'
Schlehe | Prunus spinosa
Blut-, Gold-Johannisbeere |	Ribes sanguineum, R. Aureum
Felsenbirne	| Amelanchier lamarckii, Amelanchier alnifolia 'Obelisk'
Eberesche, Vogelbeere |	Sorbus aucuparia
Speierling 	| Sorbus domestica
Weißdorn | Crataegus laevigata/monogyna
Roter Hartriegel | Cornus sanguinea
Gewöhnlicher Schneeball | Viburnum opulus
Hainbuche |	Carpinus betulus
Wolliger Schneeball	| Viburnum lantana
Schwarzer Holunder	| Sambucus nigra
Elsbeere | Sorbus torminalis
Echte Mispel | Mespilus germanica
Europäisches Pfaffenhütchen	| Euonymus europaeus
Apfeldorn |	Crataegus x lavallei'Carrierei
Winter-Linde | Tilia cordata
