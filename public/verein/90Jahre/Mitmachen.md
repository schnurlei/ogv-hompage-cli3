## Wie könnt Ihr Euch beteiligen?

1. Es wird ein Gehölz pro Haushalt in der Gemeinde Roggenburg bezuschusst. Der Baum wird auch im Gebiet der Gemeinde Roggenburg gepflanzt.
2. Nur Obstgehölze und einheimische Gehölze werden gefördert.(s. [Gehölzliste](#/90jahre/Gehoelzliste) )
3. Wir unterstützen den Kauf bis max. 35 Euro. Es wird kein Rest­betrag ausgezahlt.
4. Werden mehr als 90 Bäume beantragt, gilt die Reihenfolge des Eingangs.
5. Die Abwicklung des Kaufs erfolgt über die Baumschule Stölzle in Illertissen.
6. Der Zuschuss muss bis spätestens 01.10.2021 beim 1. Vor­sit­zen­den des OGV Schießen beantragt werden.

**Adresse:**  
Obst- und Gartenbauverein Schießen  
Rainer Schneider  
Kirchplatz 8  
89297 Roggenburg
   
**E-Mail:**  
erster.vorsitzender@ogv-schiessen.de

[Download Antrag](/verein/90Jahre/90Jahre90BaeumeFlyer.pdf)

