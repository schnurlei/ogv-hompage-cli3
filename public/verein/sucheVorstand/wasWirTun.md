### Was wir tun:

|                    |                                                                                                                                                       | 
|--------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------| 
| **Wir informieren:**| Wir bieten regelmäßig Baumschnitt– und Gartenpflegekurse an                                                                                           | 
| **Wir gestalten:**    | Wir pflanzen Bäume, säen Blumenwiesen aus und pflanzen Rabatten an                                                                                    | 
| **Wir pflegen:**       | Wir betreuen Streuobstwiesen – eines der wertvollsten Biotope in unserer Heimat                                                                       | 
| **Wir erhalten:**      | Wir helfen mit, Traditionen in unserer Gemeinde zu erhalten  wie z B. beim Gestalten des Fronleichnamsaltar und mit der  Teilnahme am Weihnachtsmarkt | 
| **Wir bilden:**        | Wir bieten regelmäßig Angebote für Kindern an                                                                                                         |

