Hinter unserem Verein steht ein starker Kreis- und Landesverband, der uns in allen
Fragen unterstützt.
Es sind absolut keine Vorkenntnisse nötig, um sich bei uns zu engagieren, nur
Begeisterung für Garten und Natur.

Wir sind in den Ortsteilen Ingstetten, Roggenburg, Schleebuch, Schießen und
Unteregg tätig und würden uns freuen, wenn sich aus jedem Ortsteil engagierte
Bürger für unsere Vorstandschaft finden würden.

Insbesondere möchten wir neue
Bürger der Gemeinde dazu aufrufen, Ihre Ideen bei uns einzubringen. Ein frischer
Blick kann einen wertvollen Beitrag zu unserer Arbeit leisten.
Wir freuen uns darauf, von Ihnen zu hören

Rainer Schneider, 
✉ erster.vorsitzender@ogv-schiessen.de
☎ 0151/20147924