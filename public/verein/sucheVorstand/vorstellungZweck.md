### Liebe Vereinsmitglieder, liebe MitbürgerInnen,

die ganze Welt retten können wir als Gartenbauverein natürlich nicht, aber einen
kleinen Beitrag dazu können wir leisten. Dazu brauchen wir aber Ihre Mithilfe. Um
die gärtnerische Vielfalt in unserer Gemeinde weiter zu fördern und zu pflegen, sind
wir auf der Suche nach neuen Mitgliedern in unserem Vorstands-Team.
