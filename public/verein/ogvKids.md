### Kindern Wurzeln geben

> _Wenn Kinder jung sind gib ihnen Wurzeln, wenn Sie alt sind gib ihnen Flügel_

Als Gärtner wissen wir, dass jede Pflanze Wurzeln braucht. 
Und eine Wurzel wiederum besteht aus vielen großen und kleinen Teilen.
Genauso brauchen Kinder große und kleine Wurzeln. Wir als Obst- und Gartenbauverein möchten dazu unseren 
Beitrag leisten. Kinder sollen die Natur und einen Garten mit allen Sinnen erfahren.
Eine Erdbeere schmecken, einen Schmetterling beobachten, die unterschiedlichen Strukturen von Blättern ertasten oder den
Tieren im Wald zuhören. Den Erfolg erleben, wenn man eine eigene Pflanze vom Samenkorn großzieht. Das sollte jedes Kind
einmal erleben. Aber auch lernen, dass nicht immer alles so funktioniert, wie man es geplant hat.

### Was machen wir

Bei den OGV Kids bieten wir in unregelmäßigen Abständen im Jahr Angebote für Kinder an.
* Apfelsaft pressen
* Mit Naturmaterialien basteln
* Hauswurz pflanzen
* Insektenhotel basteln

 