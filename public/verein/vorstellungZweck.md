### Geschichte und Zweck des Vereins

Der Obst- und Gartenbauverein Schießen wurde 1930 als Obstbau- und Bienenzuchtverein gegründet. 

Die Satzung des Vereins gibt folgenden Vereinszweck an:

> _... Der Verein bezweckt im Rahmen der Gartenkultur und der Landespflege die Förderung des Umweltschutzes zur Erhaltung 
> einer schönen Kulturlandschaft und der menschlichen Gesundheit.   
> Der Verein unterstützt insbesondere die Ortsverschönerung und dient damit der Verschönerung der Heimat,
> der Heimatpflege und somit der gesamten Landeskultur. ...* (§2 Abs.1 Satzung)_

> _... erstreckt seine Tätigkeit auf das Gebiet der Orte Schießen Schleebuch und Unteregg, bei Bedarf auch auf Roggenburg und Ingstetten ...* (§1 Satzung)_
