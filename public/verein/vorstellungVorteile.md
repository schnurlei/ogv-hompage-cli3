### Vorteile als Mitglied

* Beratung in Gartenfragen durch unsere Gartenpfleger und [Kreisfachberater](http://www.landkreis.neu-ulm.de/de/kreisfachberatung/kreisfachberatung-20001151.html "Kreisfachberater")
* Schnitt- und Pflanzkurse sowie Gartenbegehungen
* Ausleihmöglichkeit von Gartengeräten
* Vereinsausflüge und Lehrfahrten
* Teilnahme an Fachvorträgen, Seminaren und Kursen
* Erfahrungsaustausch und Geselligkeit
* Fachinformationen des [Dachverbandes](http://www.gartenbauvereine.org/fachinformationen/ "Dachverband")
