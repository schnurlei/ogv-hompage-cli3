## Projekt im Landkreis Neu-Ulm
### „Blühende Landschaft durch artenreiche Wildblumenwiesen“


Bienen, Hummeln, Schmetterlinge und Co. sind unersetzlich, denn sie sorgen als Blütenbestäuber für die Vielfalt von Pflanzen und Tieren. Auf Feldern, öffentlichen Flächen und in Gärten blüht es immer weniger. Der rasant zunehmende Artenrückgang ist Folge dieser negativen Entwicklung.

**Wir wollen das ändern!**

Zur Förderung der Artenvielfalt im Landkreis wird von unserem Kreisverband Neu-Ulm auch in diesem Jahr wieder Wildblumensaatgut kostenlos an Gartenbesitzer im Landkreis Neu-Ulm weitergegeben. 

**Bitte machen Sie mit!**

Durch die Aussaat von Wildblumenwiesen sollen im ganzen Landkreis Neu-Ulm möglichst viele neue Lebensräume für Insekten geschaffen und bestehende aufgewertet werden.

- Bitte melden Sie sich bis spätestens 18. April, wenn Sie im Landkreis Neu-Ulm triste Grünflächen in arten­reiche Blühflächen verwandeln wollen
- Sie erhalten von uns kostenlos Wildblumensaatgut für bis zu 100 m² Fläche und ei­ne Saatanleitung
- Die Blühmischung ist für eine Standzeit von fünf Jahren ausgelegt

Wer Interesse hat, bitte melden für Biberach bei Thomas Miller 07300 921056 und für Schießen bei Rainer Schneider 07300 249412 oder erster.vorsitzender@ogv-schiessen.de.

**Saatablauf Anfang/Mitte Mai**

1. Boden feinkrümlig vorbereiten (keinesfalls Saatgut einfach auf Grasflächen streuen)
2. Saatgut obenauf aussäen (Kleinflächen 2 Gramm/qm – größere Flächen 1 Gramm/qm)
3. zur Erleichterung der Aussaat das Saatgut mit der 4 fachen Menge Sand/Sägemehl strecken
4. Ausgesätes Saatgut keinesfalls einarbeiten
5. Saatbeet abschließend walzen, damit das Saatgut guten Bodenschluss erhält
6. Saatbeet während der Keimphase feucht halten (i.d.R. 14 Tage)


[Download Saatanleitung](/news/SaatanleitungBlumenwiesen.pdf)
