Im Rahmen der Klimaschutzziele 2030 plant das Bundesministerium für Ernährung und Landwirtschaft den Torfausstieg für Deutschland. Kommunen können dabei einen wichtigen Beitrag leisten. Die Stadtgärtnerei Kempten im Allgäu geht bereits seit über 10 Jahren mit gutem Beispiel voran. Alle Grünflächen, Sportanlagen und die über 30 Parks im Stadtgebiet wurden nach und nach auf eine torffreie Bewirtschaftung umgestellt.

## Weitere Informationen    

[Reportage-Reihe "Torffreies Gärtnern in der Stadt Kempten"](https://www.kempten.de/reportage-reihe-torffreies-gartnern-in-der-stadt-kempten-36984.html)