Am Tag der deutschen Einheit letzten Jahres wurde der erste Premiumwanderweg in Bayerisch-Schwaben feierlich eröffnet.

Der Wanderweg wurde im September vom Deutschen Wanderinstitut erfolgreich zertifiziert.

Die Beschreibung des Wanderweges findet sich auf der der Homepage des [Donautal-Aktiv e.V.](https://donautal-touren.de/donauwald-wanderweg/)

Dort findet man auch Informationen zum **[Radelspaß am 19./20. September 2020 in Wertingen](https://donautal-radelspass.de/)**,
dem größten Genussradeltag in Bayern


[Homepage](https://donautal-touren.de/)

