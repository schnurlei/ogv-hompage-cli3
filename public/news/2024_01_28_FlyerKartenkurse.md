Bei Gartenkursen lernen Gartenbesitzer, wie und wann die Sprösslinge im eigenen Garten gepflegt werden müssen. Die fachgerechte Pflege fördert die Gesundheit, Blütenvielfalt, Fruchtbarkeit und Langlebigkeit unserer Gartenpflanzen.
Folgende Gartenkurse finden unter der Leitung von Herrn Rudolf Siehler, Kreisfachberater für Gartenkultur und Landespflege im Landkreis Neu-Ulm, statt.
- Schnittkurse bei Gartenbauvereinen in den Gemeinden im Landkreis
- Gartenkurse im Kreismustergarten im Rahmen des VHS Programms
- Führungen im Kreismustergarten mit Tipps zur Gartenanlage und Pflege

[Download Flyer](/news/FlyerGartenkurseKvNU2024.pdf)