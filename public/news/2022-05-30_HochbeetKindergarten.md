Die **Raiffeisenbank Mittelschwaben** spendete dem Kindergarten in Schießen 3 Hochbeete.
Die Kinder bepflanzten die Beete unter anderem mit Erdbeeren und Kartoffeln.
Ein Teil der Pflanzen wurde dem Kindergarten vom Obst- und Gartenbauverein Schießen gespendet.

Wir wünschen eine gute Ernte.