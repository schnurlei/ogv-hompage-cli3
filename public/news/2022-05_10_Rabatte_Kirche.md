## Die Rabatte um den Parkplatz vor der Kirche wurden neu gestaltet

Entlang der Mauer wurde ein Kräuterstreifen angelegt.

Links und rechts vom Parkplatz wurde eine Weidenblättrige Birne "Pyrus salicifolia" gepflanzt.

Auf der rechten Seite entstand ein Staudenbeet mit vielen heimischen und insektenfreundlichen Stauden.

Auf der linken Seite wurde eine kleine Blumenwiese angesät.


Die Fotos zu der Rabatte sind in einer [Gallery](#/gallery/2022_RabatteKirche) zu finden.