Am 21.03.2024 fand die ordentliche Mitgliederversammlung des Obst- und Gartenbauverein Schießen im Bräuhaus statt.

Bei der Versammlung wurde eine neue Vorstandschaft gewählt.
Wir bedanken uns bei allen, die sich dazu bereit erklärt haben, in der neuen Vorstandschaft mitzuarbeiten.

Das Ergebnis der Wahl lautet:

| Position        |           Vorname | Name       |
|-----------------|------------------:|------------|
| 1. Vorsitzender |            Rainer | Schneider  |
| 2. Vorsitzende  |            Gisela | Hille-Reh  |
| Kassierer       |             Georg | Engelmayer |
| Schriftführerin |             Helga | Gass       |
| BeisitzerInnen  |            Sylvia | Lecheler   |
|                 |           Jessica | Breuel     |
|                 |              Fred | Scholz     |
| KassenprüferIn  |            Sandra | Gerstlauer |
|                 |           Dieter  | Rittler    |



Der Jahresrückblick des 1. Vorsitzenden ist unter folgendem Link zu finden:

https://rainer-exxcellent.github.io/OgvMitgliederversammlung/versammlung2024.html



Es gab auch wieder eine Blumenverlosung mit tollen Preisen.

![2024-03-21_Blumenverlosung2024.JPG](news/2024-03-21_Blumenverlosung2024.JPG)
