Zu diesem Zweck gibt es Sichtungsgärten. In diesen werden Pflanzen über mehrere Jahren hinweg auf Herz und Nieren geprüft und bewertet.

Die beste Bewertung sind dabei 3 Sterne, also ausgezeichnet. Eine Übersicht über die Sichtungsergebnisse von Stauden gibt die Seite: 

[Staudensterne](https://www.staudensterne.de/)
  


Die Sichtungsgärten sind auch immer eine Reise wert, da diese meist auch die Verwendung der Pflanzen in wunderschönen Schaugärten aufzeigen. 
Zwei der bekanntesten und schönsten dieser Gärten sind:

[Weihenstephaner Gärten](https://www.hswt.de/servicelinks/weihenstephaner-gaerten/sichtungsgarten.html)

[Hermannshof](https://sichtungsgarten-hermannshof.de/)

   



