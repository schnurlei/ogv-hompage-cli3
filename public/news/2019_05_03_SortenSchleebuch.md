Folgende Apfelbäume sind auf der Streuobstwiese zwischen Schießen und Schleebuch zu finden und können auch gerne verkostet werden.

Größere Menge dürfen nur in Absprache mit dem Obst- und Gartenbauverein Schießen entnommen werden.


| Sorte                    | Reife           | 
| -----------------------  | ---------------| 
| Pilot                    | Ende September  | 
| Jakob Fischer            | Anfang September| 
| Prinz Albrecht v. Preußen| Ende September  | 
| Jakob Lebel              | Mitte September | 
| Stark Earliest           | Anfang August   |
| Transparent von Croncels | Ende August     |
| Jamba                    | Mitte August    |
| Pfaffenhofer Schmelzling | Ende September  | 
| Brettacher               | Mitte Oktober   | 
| Geheimrat Dr. Oldenburg  | Mitte September | 
| Wettringer Taubenapfel   | Mitte Oktober   | 
| Alkmene                  | Mitte September |

