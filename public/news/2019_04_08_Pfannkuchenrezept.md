### Zutaten

| Menge        | Zutat       | 
| -----------: | -----------:| 
| 680 g        | Weizenmehl  | 
| 720 g        | Dinkelmehl  | 
| 150 g        | Emmermehl   | 
| 40 Stck      | Eier        | 
| 4,5 l        | Milch       |
|              | Salz        |
|              | Wildkräuter |

###   Zubereitung

Mehl, Eier, Milch und Salz miteinander zu einem glatten Teig verrühren

Teig einen Wildkräuterspaziergang lang quellen lassen (ca. 45 min)

Wildkräuter (Girsch, Bärlauch, Löwenzahn, ...) klein hacken und mit dem Teig vermengen

Holz in eine alte Waschmaschinentrommel geben und entzünden

Öl in der Pfanne über dem Feuer erhitzen

Pfannkuchen im Öl ausbacken und mit Gänseblümchen dekorieren

Guten Appetit
    