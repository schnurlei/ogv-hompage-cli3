Am 'Research Institute for Agriculture, Fisheries and Food' in Belgien wurden 4 neue Buchsorten gezüchtet,
die resistent gegen den Buchsbaumpilz 'Calonectria pseudonaviculata' (früher Cylindrocladium buxicola) sein sollen.
Die neue Sorten werden unter anderem im Schloss Villandry in Frankreich getestet.

[bruns.de](https://www.bruns.de/neuigkeiten/newsdetails/value/resistenter-buxus-neuheit-exklusiv-bei-bruns/)
[betterbuxus.com](https://betterbuxus.com/?lang=de)

Auch in den USA wurden zwei neue Sorten gezüchtet. Diese werden unter anderem im Rosengarten des Weißen Haus getestet.

[newgenboxwood.com](https://www.newgenboxwood.com/)