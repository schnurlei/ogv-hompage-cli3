## Der hässlichste Rasen der Welt wurde gewählt

Die Gewinnerin des Wettbewerbs „Der hässlichste Rasen der Welt“ ist Kathleen Murray aus Tasmanien, Australien!

Nachdem die internationale Jury aus der ganzen Welt Beiträge zum Thema „Hässlicher Rasen“ erhalten hatte, hat sie nun den Titel „Der hässlichste Rasen der Welt“ gekürt. Es war keine leichte Aufgabe, da alle Einträge hässlich waren, aber ein Rasen war tatsächlich der hässlichste.

Es ist das schönste Beispiel für den unattraktivsten Rasen für den wertvollsten Zweck der Welt: den Wasserschutz. Das ist das Geniale an diesem Wettbewerb. Es nutzt Humor, um ein so ernstes Thema zu beleuchten. Und es belohnt diejenigen, die stolz verkünden wollen, dass ihr Rasen die angewiderten Blicke der Nachbarn verdient – ​​und einen Applaus aus der ganzen Welt.

[Homepage Wettbewerb](https://gotland.com/worlds-ugliest-lawn/)