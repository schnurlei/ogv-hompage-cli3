## Querbeet zu Gast in Oberelchingen 

Die BR Sendereihe Querbeet zeigte am 15.11.  eine tolle Sendung über den Klostergarten Oberelchingen.

Im Rahmen einer Pflanzaktion mit Kindern und Jugendlichen wurden dabei unter Anleitung der Gartenbauvereinsvorsitzenden Sigrid Hiller 3000 Blumenzwiebeln gepflanzt.

Die Sendung stellt diese tolle Aktion vor und zeigt schöne Filmausschnitte der sehenswerten Klosteranlage.

[Link zur Sendung](https://www.br.de/br-fernsehen/sendungen/querbeet/querbeet-klostergarten-elchingen-blumenzwiebeln-100.html)

