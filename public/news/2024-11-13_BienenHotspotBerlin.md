
Prof. Dr. Norbert Kühn vom Institut für Landschaftsarchitektur und Umweltplanung der Technischen Universität Berlin berichtet in einem sehr lesenswerten Artikel in der Zeitschrift "Stadt + Grün" über Wildbienen in der Stadt.

Er zeigt dabei sehr anschaulich auf, was Wildbienen benötigen, um in der Stadt und auf dem Land zu überleben.




[Artikel in der Zeitschrift Stadt + Grün](https://stadtundgruen.de/artikel/pflanzen-zur-foerderung-von-wildbienen-in-der-stadt-bienen-hotspot-berlin-5257)