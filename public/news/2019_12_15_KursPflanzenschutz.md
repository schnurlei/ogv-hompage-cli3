Die Hochschule Weihenstephan-Triesdorf hat Ihren ersten OPEN vhb-Kurs online gestellt:

**Pflanzenschutz – gefährlich, sinnlos und überflüssig? Eine Einführung in die Welt der Schaderreger an Pflanzen.**

Der Kurs ist kostenfrei und erfordert lediglich eine Registrierung mittels Mailadresse auf der OPEN vhb-Seite 

[Presse-Mitteilung](https://www.hswt.de/forschung/news/article/erster-open-vhb-kurs-der-hochschule-weihenstephan-triesdorf-startet-mit-dem-thema-pflanzenschutz.html)

[Kurs Pflanzenschutz](https://open.vhb.org/blocks/ildmetaselect/detailpage.php?id=118)
