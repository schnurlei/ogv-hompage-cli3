## Projekt im Landkreis Neu-Ulm
### „Blühende Landschaft durch artenreiche Wildblumenwiesen“


Bienen, Hummeln, Schmetterlinge und Co. sind unersetzlich, denn sie sorgen als Blütenbestäuber für die Vielfalt von Pflanzen und Tieren. Auf Feldern, öffentlichen Flächen und in Gärten blüht es immer weniger. Der rasant zunehmende Artenrückgang ist Folge dieser negativen Entwicklung.

**Wir wollen das ändern!**

Zur Förderung der Artenvielfalt im Landkreis wird von unserem Kreisverband Neu-Ulm auch in diesem Jahr wieder Wildblumensaatgut kostenlos an Gartenbesitzer im Landkreis Neu-Ulm weitergegeben. Die Resonanz auf unseren Aufruf im vergangenen Jahr war hervorragend und wird fortgeführt. Bisher konnten mit der kostenlosen Weitergabe von 20 Kilogramm Blumensaatgut über 10.000 m² neue Blumenwiesen geschaffen werden.

**Bitte machen Sie mit!**

Durch die Aussaat von Wildblumenwiesen sollen im ganzen Landkreis Neu-Ulm möglichst viele neue Lebensräume für Insekten geschaffen und bestehende aufgewertet werden.

    • Bitte melden Sie sich, wenn Sie im Landkreis Neu-Ulm triste Grünflächen in artenreiche Blühflächen verwandeln wollen
    • Sie erhalten von uns kostenlos bis zu 100 Gramm Wildblumensaatgut und eine Saatanleitung
    • Die Blühmischung ist für eine Standzeit von fünf Jahren ausgelegt
    
Wer Interesse hat, bitte melden!

    • Kreisfachberater für Gartenkultur und Landespflege (0731/7040-4307)
    • Vorsitzende der örtlichen Obst- und Gartenbauvereine