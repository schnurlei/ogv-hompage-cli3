Auf der [Neuigkeiten-Seite](/#/news) wollen wir regelmäßig über Neuigkeiten aus unserem Verein und aus der Welt des Gärtners berichten.
Auf die Seite kann direkt über das Icon in der Kopfzeile navigiert werden.   

**Woher kommen die Icons auf unserer Homepage?:**

Für die Icons in unserer Homepage verwenden wir die Bibliothek von [Font Awesome](https://fontawesome.com/)