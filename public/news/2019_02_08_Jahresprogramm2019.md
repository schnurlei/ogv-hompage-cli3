Das Jahresprogramm enthält Informationen über alle Aktionen und Termine unseres Kreisverbandes 
und eine Übersicht aller Kurse, die von unserer Kreisfachberatung vor Ort durchgeführt werden.

Über folgenden Link steht es zum Download bereit. 


[Jahresprogramm 2019](http://www.landkreis.neu-ulm.de/datei/anzeigen/id/14895,1060/jahresprogramm_kreisverband_gartenbau_nu_2019.pdf) 