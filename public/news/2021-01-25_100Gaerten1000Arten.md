Die Kampagne 

https://www.tausende-gaerten.de/

will mehr biologische Vielfalt in Deutschland fördern.

Dazu bietet Sie auf ihrer Homepage eine Vielzahl von Informationen zum naturnahen Gärtnern.


_“Einen Garten zu bepflanzen bedeutet, an morgen zu glauben.”_

Audrey Hepburn
