2021 finden in Bayern und Baden-Württemberg 4 Gartenschauen statt:

https://ingolstadt2020.de/

https://www.lindau2021.de/

https://www.ueberlingen2020.de/

https://www.gartenschau-eppingen.de/

Zusätzlich findet auch die Bundesgartenschau in Erfurt statt:

https://www.buga2021.de/

In der Nähe von Erfurt ist auch die weltgrößte Rosensammlung in Sangerhausen beheimatet.

https://europa-rosarium.de/