## OGV Schießen im Bayerischen Fernsehen

Am 12.11.2021 war das Bayerische Fernsehen in Roggenburg, um über unsere Aktion 90 Jahre – 90 Bäume zu berichten.

Wir haben an diesem Tag in mehreren Gärten in Schießen und Schlee­buch unterschiedliche Bäume gepflanzt.

Das Team des Bayerischen Fernsehen hat uns dabei begleitet und 4 Stunden lang gedreht.
Aus diesen Dreharbeiten entstand eine 4,5 min lange Do­ku­men­ta­tion.

Diese wurde am 13.12.2021 um 19:00 Uhr in der Sendung Querbeet im Bayerischen Fernsehen ausgestrahlt und ist auch in der Mediathek zu sehen. 

[Ausstrahlung Querbeet](https://www.br.de/br-fernsehen/sendungen/querbeet/querbeet-vereinsjubilaeum-neue-baeume-100.html)