Auskunft über den Veranstaltungsort in den Gemeinden geben die
Vorsitzenden der örtlichen Gartenbauvereine. Bei schlechter Witterung erteilen diese auch Auskunft darüber, ob der geplante Kurs witterungsbedingt stattfinden kann.

Die Gartenkurse sind kostenlos und können von jedem interessierten 
Gartenbesitzer besucht werden. Jeder Teilnehmer lernt dabei vom Fachmann Schritt für Schritt die Grundlagen der fachgerechten Gartenpflege.

Nähere Hinweise über den jeweiligen Kursinhalt erteilen die Kreisfachberater für Gartenkultur und Landespflege im Landratsamt Neu-Ulm.

| Datum        | Uhrzeit  | Kursthema        | Gemeinde        |
| -----------: | --------:| ---------------- | ----------------|
| 01.02.19     | 13:30    | Obstbaumschnitt  | Straß           |
| 09.02.19     | 09:00    | Obstbaumschnitt  | Illereichen     |
| 15.02.19     | 14:00    | Obstbaumschnitt  | Unterelchingen  |
| 22.02.19     | 13:30    | Obstbaumschnitt  | Pfuhl           |
| 23.02.19     | 09:00    | Obstbaumschnitt  | Jedesheim       |
| 01.03.19     | 14:00    | Obstbaumschnitt  | Illertissen     |
| 09.03.19     | 09:00    | Obstbaumschnitt  | Schießen        |
| 15.03.19     | 14:00    | Obstbaumschnitt  | Pfaffenhofen    |
| 22.03.19     | 14:00    | Gartenpflegekurs | Vöhringen       |
| 30.03.19     | 09:00    | Gartenpflegekurs | Unterroth       |
| 02.04.19     | 17:00    | Rosenschnittkurs | Biberachzell    |
| 02.07.19     | 18:00    | Sommerschnittkurs| Finningen       |
