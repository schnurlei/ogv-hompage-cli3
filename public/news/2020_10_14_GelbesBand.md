Der Obst- und Gartenbauverein Biberach-Asch hat das Projekt gelbes Band gestartet.
Bäume, die von jedermann beerntet werden dürfen, sind zwischen Biberach und Biberachzell gekennzeichnet worden.

Vorbild ist das Projekt des Landkreis Esslingen:

[Zu gut für die Tonne](https://www.zugutfuerdietonne.de/der-bundespreis/2020/gelbes-band-das-ernteprojekt/)


[Stadtfrüchte](https://www.schwaebischhall.de/de/unsere-stadt/klimaschutz-energie/mach-mit/stadtfruechte)