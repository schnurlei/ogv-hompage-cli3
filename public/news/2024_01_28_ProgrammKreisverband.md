In der zweiten Januarwoche spürt man schon die zunehmende Tageslänge.
Wir freuen uns alle darüber und planen bereits für das anstehende Gartenjahr 2024.

Unser Jahresprogramm des Kreisverbands für Gartenbau Neu-Ulm ist fertiggestellt und im Anhang beigefügt.

Es informiert Sie wieder umfassend über

- unsere gemeinsamen Umweltprojekte
- Termine der Gartenkurse in den Gemeinden
- das Kursangebot der Volkshochschule im Mustergarten
- und vieles mehr…

Sie finden das Jahresprogramm und viele weitere Informationen zu den verschiedenen Umweltprojekten des Landkreises auf unserer Homepage:

[Homepage Kreisverband](https://www.landkreis-nu.de/de/Service-Verwaltung/Unsere-Fachbereiche/Natur-Umwelt/Kreisverband-Gartenbauvereine)

 