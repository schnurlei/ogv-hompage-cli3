Am 07.08.2022 fand der **ILE – Familien- und Freizeittag 2022** statt.
Im gesamten ILE Gebiet wurden viele Aktionen für die ganze Familie geboten. 

Gemeinsam mit dem OGV Biberach/Asch hat der OGV Schießen zum Gelingen dieses Tages beigetragen.

Wir bastelten Papierflieger mit den Kindern und jeder Besucher konnten seinen eigenen Kräuterbuschen binden und mit nach Hause nehmen. Die Aktion fand so großen Anklang, dass wir am Ende komplett 'ausverkauft' waren. 

[ILE - Auf Entdeckertour in unserer Region](https://www.ile-iller-roth-biber.de/region-erkunden/auf-entdeckertour-in-unserer-region/)
