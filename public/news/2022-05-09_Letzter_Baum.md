## Abschluss der Aktion 90 Jahre - 90 Bäume 

Zum Abschluss unserer Aktion '90 - Jahre 90 Bäume' haben wir den 90. Baum zusammen mit unseren Kommunionkindern gepflanzt.
Danach wurde das gelungene Projekt noch bei einem kleinen Sektempfang gefeiert.

Wie bedanken uns bei allen Helfern, die zum Gelingen der Aktion beigetragen haben. Vor allen bedanken wir uns aber bei der Gemeinde Roggenburg für die Unterstützung.

Die Fotos zu der Veranstaltung sind in einer [Gallery](#/gallery/2022_Abschluss90Jahre) zu finden.