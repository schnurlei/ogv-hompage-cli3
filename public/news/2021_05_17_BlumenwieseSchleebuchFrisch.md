## Blumenwiese in Schleebuch

In Zusammenarbeit mit der Gemeinde Roggenburg hat der OGV Schießen eine Blumenwiese in Schleebuch angesät.
Wir hoffen auf eine vielfältige und lebendige Blühfläche, an der Bienen und Schmetterlinge ihre Freude haben. 