Folgende Gartenschauen können dieses Jahr in Bayern und Baden-Württemberg besucht werden: 



| von      | bis        |  Homepage|
| --------:| ----------:| ---------------- | 
| 17.04.19 | 06.10.2019 | [Bundesgartenschau Heilbronn](https://www.buga2019.de/de/index.php)|
| 10.05.19 | 20.10.2019 | [Remstal Gartenschau](https://remstal.de/home.html) |
| 24.05.19 | 08.09.2019 | [Landesgartenschau in Wassertrüdingen](https://www.wassertruedingen2019.de/)|

