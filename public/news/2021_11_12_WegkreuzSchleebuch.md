## Winterlinden am Wegkreuz 

Durch die Zusammenarbeit von 3 Vereinen in unserer Gemeinde ist ein schöner neuer Platz entstanden.

Das Wegkreuz an der Straße zwischen Roggenburg und Biberach war nicht mehr standsicher. Deswegen wurde es vom **Verein für Heimatpflege Roggenburg** in die Nähe des neuen Hochbehälter in Schleebuch versetzt. Zusätzlich wurden vom **Obst- und Gartenbauverein Biberach-Asch** und vom  **Obst- und Gartenbauverein Schießen** zwei Winderlinden links und rechts vom Kreuz gepflanzt.

