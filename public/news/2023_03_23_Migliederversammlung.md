Am 23.03.2023 fand die Mitgliederversammlung des Obst- und Gartenbauverein Schießen im Bräuhaus statt.
Es gab wieder eine Blumenverlosung mit tollen Preisen.

Der Jahresrückblick des 1. Vorsitzenden ist unter folgendem Link zu finden:

https://rainer-exxcellent.github.io/OgvMitgliederversammlung2023/


![Blumenverlosung2023.JPG](news/Blumenverlosung2023.JPG "a title")
