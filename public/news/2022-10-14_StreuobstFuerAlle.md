Im Rahmen des Bayerischen Streuobstpaktes wurde das neue Förderprogramm
„Streuobst für alle!“ gestartet. Dabei wird durch das Bayerische Staatsministerium für
Ernährung, Landwirtschaft und Forsten (StMELF) der Erwerb von hochstämmigen
Streuobstbäumen zur Pflanzung mit bis zu 45 Euro je Baum gefördert.

Die Förderanträge können nicht von Einzelpersonen gestellt werden, sondern dafür sollen Kommunen, Vereine und Verbände als Bündler und Multiplikatoren fungieren. Diese können die Anträge digital bei den Ämtern für Ländliche Entwicklung einreichen.


[Streuobstpakt – Förderprogramm Streuobst für alle!](https://www.stmelf.bayern.de/agrarpolitik/foerderung/309120/)

[Auf geht’s – Streuobst für alle!](https://www.stmelf.bayern.de/landentwicklung/dokumentationen/313590/index.php)