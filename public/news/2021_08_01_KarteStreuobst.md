## Karte der Apfel- und Birnensorten im Landkreis Neu-Ulm


In den Jahren 2016 - 2019 wurden alte Apfel- und Birnensorten
im nördlichen Schwaben erfasst.

Das Ergebnis des Projektes finden Sie hier:

[Bestandsaufnahme](https://landkreis.neu-ulm.de/download/25720/projektbrosch%C3%BCre_obstsortenerfassung_nordschwaben_2016_2020_onlineversion.pdf)

Eine Karte der erfassten Sorten im Landkreis Neu-Ulm finden auf unserer Homepage

[Karte](https://www.ogv-schiessen.de/#/themen/StreuobstMap)

Weitere interessante Informationen finden Sie unter:

https://www.pomologen-verein.de/
