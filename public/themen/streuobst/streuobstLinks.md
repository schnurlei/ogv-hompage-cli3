## Informationen zum Thema Streuobst:

* [Lfl Streuobst](http://www.lfl.bayern.de/streuobst)        
* [Lwg Streuobst](http://www.lwg.bayern.de/landespflege/natur_landschaft/086526/index.php)
* [Biologischer Obstbau auf Hochstammbäumen](https://shop.fibl.org/dede/mwdownloads/download/link/id/115/)
* [Naturgemäßer Obstbaumschnitt für die Praxis](https://schlaraffenburger.de/cms/component/content/article/42-obstbaumschnitt/102-pflanzung-und-pflege-von-streuobstbaeumen.html)
* [Die Kulturgeschichte des Obstbaus](https://www.lfl.bayern.de/mam/cms07/iab/dateien/kulturgeschichte_obstbau_extern.pdf)         
* [Streuobsterfassung in Nordschwaben](https://lag-monheimeralb-altmuehljura.de/)
* [Sortenkartierung Nordschwaben Karte](https://schlaraffenburger.de/cms/index.php/sortenkartierung-nordschwaben-2016-map)     
* [LBV Streuobst](https://www.lbv.de/naturschutz/lebensraeume-schuetzen/streuobstwiesen/)
* [Pomologen-Verein e.V.](https://www.pomologen-verein.de/)
* [Streuobst Pädagogen](https://www.streuobst-paedagogen.de/)
* [Winterschnitt an Obstgehölzen]  (http://streuobst-lueneburg.de/docs/themenblaetter/Themenblatt_08-Obstgehoelzschnitt.pdf)
* [Grundlagen des Obstbaumschnitts](https://www.kreis-tir.de/fileadmin/user_upload/Gartenkultur/downloads/Obstbaumschnitt_-_eine_Anleitung.pdf)
        
        
        
        
        
        
        
        
        
        
        
        
 