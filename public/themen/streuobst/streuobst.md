## Streuobstwiesen

Streuobstwiesen sind ein prägender Bestandteil unserer dörflichen Kulturlandschaft. Sie verschönern unsere Gemeinde, bieten mehr als 5.000 Tier- und Pflanzenarten einen wertvollen Lebensraum und liefern gleichzeitig schmackhaftes Obst. Oft werden die alten Obstsorten in Streuobstwiesen von Allergikern besser vertragen als moderne Züchtungen.

Die Zukunft der Streuobstbestände hängt maßgeblich davon ab, ob genug Menschen bereit sind, Zeit und Energie in deren Pflege zu investieren. Für einen einzelnen ist die Pflege eines Baumes wenig Aufwand, in der Summe bedeutet die Pflege aber hohe Kosten. Ohne Pflege, wie einen fachgerechten Baumschnitt, vergreisen die Bäume früh und bieten kein schönes Bild.     
        
        
        
        
        
        
        
        
        
 