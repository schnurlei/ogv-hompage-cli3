Jeder, der Pestizide auf Kiesflächen oder gepflasterten Wegen ausbringt, vergiftet damit unser Grundwasser.

Dass dies nicht aus der Luft gegriffen ist, zeigt das Ergebnis der Trinkwasseruntersuchung 2016 in der Gemeinde
Roggenburg. Dort wurden erhebliche Mengen des Pflanzenbehandlungs- und Schädlingsbekämpfungsmittel an Atrazin und 
Desethyl-Atrazin nachgewiesen. Und dies, obwohl  Atrazin seit 1. März 1991 in Deutschland verboten ist.
