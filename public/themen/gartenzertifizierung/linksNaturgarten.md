## Weitere Informationen

Auf folgenden Seiten finden Sie noch mehr Informationen zum Thema Naturgarten:

* [1000 Gärten 1000 Arten](https://www.tausende-gaerten.de/)
* [Vielfaltsmacher](https://www.vielfaltsmacher.de/)
* [Wildbienenwelt](https://www.wildbienenwelt.de/)
* [NaturGarten e.V.](https://www.naturgarten.org/)