## Gartenzertifizierung „Naturgarten – Bayern blüht“

Zur Förderung von Naturgärten in ganz Bayern wurde die Gartenzertifizierung „ Bayern blüht – Naturgarten“ ins Leben gerufen. Besitzer von artenreich gestalteten und ökologisch bewirtschafteten Naturgärten können
ihren Privatgarten von einer fachkundigen Bewertungskommission, zur
Auszeichnung mit einer Gartenplakette, prüfen lassen. Wer die Kriterien
eines „ Ausgezeichneten Naturgartens“ erfüllt, darf seinen Gartenzaun mit
der Bayerischen Plakette „Naturgarten – Bayern blüht" schmücken.

**Ziel der Prämierung ist, die Biodiversität und somit den Erhalt der
heimischen Tier- und Pflanzenwelt in bayerischen Gärten zu fördern.**

Um die Gartenplakette „Bayern blüht – Naturgarten“ zu erhalten müssen alle der Kernkriterien und mehrer Kann-Kriterien erfüllt werden.       
        
[Detailliert Informationen vom Landesverband](https://www.gartenbauvereine.org/fachinformationen/gartenzertifizierung/)   
       
        
        
        
        
        
