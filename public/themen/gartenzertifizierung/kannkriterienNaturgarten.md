## Kann-Kriterien – die „Kür“ im Naturgarten

### Naturgartenelemente

* Wildes Eck
* Zulassen von Wildkraut
* Wiese und Wiesenelemente
* Vielfalt an Lebensräumen
* Laubbäume
* Blumen und blühende Stauden - Insektennahrungspflanzen
* heimische Gehölze        
