### Ökologische Bewirtschaftung & Nutzgarten:
    
* Gemüsebeet & Kräuter
* Komposthaufen
* Mischkultur – Fruchtfolge – Gründüngung – Mulchen
* Nützlingsunterkünfte
* Obstgarten & Beerensträucher
* Regenwassernutzung & Bewässerung      
* Umweltfreundliche und regionaltypische Materialwahl
