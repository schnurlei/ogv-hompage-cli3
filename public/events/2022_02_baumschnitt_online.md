Da es zur Zeit leider nicht möglich ist, einen Schnitt­kurs im Freien anzu­bieten, wollen wir trotzdem nicht darauf verzichten, Ihnen ein wenig Wissen zum  Obst­baum­schnitt zu vermitteln.

Aus diesem Grund lädt Sie der Obst- und Gartenbau­ver­ein Schie­ßen herzlich zu ei­nem On­line Kurs ein.

In dem Kurs werden die wichtigsten Grundbegriffe aus dem Obst­baumschnitt vermittelt.

Bitte melden Sie sich über die E-Mail Adresse streuobst@ogv-schiessen.de für den Kurs an. Den Link zu der Zoom Ver­an­stal­tung erhalten Sie rechtzeitig vor der Veranstaltung.

**Ansprechpartner und Anmeldung:**

Rainer Schneider  
Email: streuobst@ogv­-schiessen.de

