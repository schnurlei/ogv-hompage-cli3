## Bayerische Landesgartenschau Freyung
### Wald. Weite. Wunderbar.

Die höchstgelegene Landesgartenschau Bayerns lädt Sie ein, den Blick in die Ferne schweifen zu lassen und im Herzen des Bayerischen Waldes Ruhe zu finden.
Erleben Sie die vielfältige Flora und Fauna dieser Region, deren Bewohner und Traditionen schon immer eng mit ihrer Landschaft verbunden waren.