## Mitgliederversammlung 2021

Leider entfällt die Versammlung aufgrund mehrerer Krankheitsfälle.  Wir bedauern die Absage sehr und hoffen die Veranstaltung möglichst bald nachzuholen.

Ihre Vorstandschaft
