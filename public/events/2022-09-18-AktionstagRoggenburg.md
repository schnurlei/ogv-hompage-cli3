Anlässlich des 50-jährigen Bestehens der Gemeinde Roggenburg findet unter dem Motto „Roggenburger Vielfalt“ am Sonntag, 18. September, ein bunter Aktionstag im gesamten Gemeindegebiet Roggenburg statt. 

Der OGV Schießen bietet folgende Aktionen an:

_13:00 - 15:00 Uhr - Biohof Engelmayer, Biberacher Str. 5_   
- **Jagua Tattoos** - Tattoos wie echt, aber nach 2 Wochen verschwunden

- **Samenbomben basteln**

_16:00 Uhr - Kirchplatz 8, Garten Rainer Schneider_
- **Botanisch-Alkoholischer Gartenspaziergang** - „Welche Pflanze steckt in welchem Getränk?“