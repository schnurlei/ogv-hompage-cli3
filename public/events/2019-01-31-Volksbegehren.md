Vom 31. Januar 2019 bis zum 13. Februar 2019 müssen sich eine Million Wahlberechtigte persönlich in den Rathäusern in Listen eintragen, um das Volksbegehren Artenvielfalt erfolgreich zu machen. Online ist dies nicht möglich. Zur Eintragung muss der gültige Ausweis vorgelegt werden.


**31.01.** 	08.00 - 12.00 Uhr 	16.00 - 18.00 Uhr

**01.02.**  08.00 - 12.00 Uhr

**04.02.**  08.00 - 12.00 Uhr   13.00 - 16.00 Uhr

**05.02.**  08.00 - 12.00 Uhr   13.00 - 17.00 Uhr

**06.02.**  08.00 - 12.00 Uhr   13.00 - 16.00 Uhr

**07.02.** 	08.00 - 12.00 Uhr 	13.00 - 20.00 Uhr

**09.02.**  09.00 - 11.00 Uhr

**11.02.**  08.00 - 12.00 Uhr   13.00 - 16.00 Uhr

**12.02.**  08.00 - 12.00 Uhr   13.00 - 17.00 Uhr

**13.02.**  08.00 - 12.00 Uhr   13.00 - 16.00 Uhr