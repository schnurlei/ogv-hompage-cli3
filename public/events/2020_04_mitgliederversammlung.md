## Die Mitgliederversammlung des Obst- und Gartenbauvereins Schießen entfällt aufgrund der Corona-Epidemie

Liebe Mitglieder des Obst- und Gartenbauvereins Schießen,

aufgrund der Corona-Epidemie verschieben wir unsere Mitgliederver-
sammlung auf unbestimmte Zeit. Wir hoffen, die Versammlung möglichst
bald nachholen zu können und werden Euch informieren, wenn es soweit
ist.
Wir wünschen Euch Gesundheit und alles Gute für die nächsten Wochen.


Mit gärtnerischen Grüßen

Rainer Schneider
1. Vorsitzender
