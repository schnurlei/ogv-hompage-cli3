Der Wonnemonat Mai erfreut mit seiner ganzen Blütenpracht Mensch und Natur.

Täglich kann man neue Blüten entdecken, Pflanzen beim Wachsen beobachten und immer mehr Tiere im Garten sehen und hören.

Jetzt ist die beste Jahreszeit um die Natur in vollen Zügen zu genießen und die ganze Blütenpracht und Artenvielfalt in Gärten, blühenden Streuobstwiesen und in der Landschaft zu erleben.

Passend zur Jahreszeit lädt die „Die Woche der Artenvielfalt“ vom 14. bis 22. Mai 2023 wieder zum Mitmachen und Erleben ein.

Nach dem tollen Erfolg in 2022 laden in bewährter Zusammenarbeit der Botanische Garten Ulm und unser Landkreis Neu-Ulm übernächste Woche täglich zum Besuch von spannenden kostenlosen Veranstaltungen, Workshops und Exkursionen ein.

Unter dem Motto “Artenvielfalt hinter den Kulissen“ stellen wir dabei allen Naturliebhaber*innen am 16.5.23 auch unseren artenreichen Kreismustergarten mit seinem neuen Umweltprojekt „Erhaltungsgarten bedrohter Apfel- und Birnensorten“ vor.


[Weitere Informationen](https://www.landkreis-nu.de/Woche-der-Artenvielfalt)