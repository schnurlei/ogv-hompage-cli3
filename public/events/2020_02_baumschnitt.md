Die Obst- und Gartenbauvereine Biberach-Asch und Schießen bieten gemeinsam einen Baumschnittkurs an der Streuobstwiese bei Ingstetten an. Dort befinden sich Obstbäume unterschiedlichen Alters, an denen wir die Schnittmaßnahmen demonstrieren wollen.

Wir wollen den Teilnehmern grundlegende Kenntnisse zum Thema Obst­baumschnitt vermitteln. Insbesondere wollen wir dabei auf den Schnitt alter Obstbäume eingehen, die evtl. schon länger nicht gepflegt wur­den. Die Teilnehmer dürfen während des Kurses auch selber Hand an­le­gen und eigene Erfahrungen beim Schnitt sammeln.

Bitte bringen Sie Arbeitshandschuhe und eigene Schnittwerkzeuge mit, so­fern vorhanden. Denken Sie bitte auch an festes Schuhwerk und an­ge­messene wetterfeste Kleidung. Die Teilnahme ist kostenlos.


**Ansprechpartner und Anmeldung:**

Thomas Miller und Rainer Schneider  
Email: streuobst@ogv­-schiessen.de

