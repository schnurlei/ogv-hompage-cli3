## Mitgliederversammlung 2019

**Tagesordnung**

    1. Eröffnung und Begrüßung
    2. Bericht des 1. Vorsitzenden
    3. Bericht der Schriftführerin
    4. Bericht der Kassiererin
    5. Entlastung der Kassiererin und der Vorstandschaft
    6. Grußworte der anwesenden Gäste
    7. Ehrungen


Im Anschluss an den offiziellen Teil findet wieder unsere **traditionelle Blumenverlosung** statt.

Auf Ihr zahlreiches Kommen freut sich die Vorstandschaft