Wie schon 2020 entfällt der Tag der offenen Gartentür 2021 aufgrund der Corona-Pandemie leider.
2022 wird ein neuer Anlauf gestartet.

**Offizielle Information:**

 Aufgrund der derzeitigen Situation ist eine Durchführung unter einigermaßen normalen Bedingungen wohl wenig wahrscheinlich. Aus unserer Überzeugung sind etwaige Einschränkungen bezüglich Kontakt und Verweildauer weder dem Besucher und schon gar nicht dem Gartenbesitzer zuzumuten. Darum sehen wir in Absprache mit dem Gartenbauzentrum Süd- West von einer Veranstaltung ab und müssen nun leider schon zum zweiten Mal die Aktion absagen.

Wir bitten dafür um Ihr Verständnis.