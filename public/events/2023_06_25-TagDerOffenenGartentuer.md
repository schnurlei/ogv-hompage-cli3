
Der "Tag der offenen Gartentür" bietet wieder die Möglichkeit, Gärten kennen zu lernen und mit ihren Besitzerinnen ins Gespräch zu kommen.
Es ist eine schöne Gelegenheit, um sich Anregungen für den eigenen Garten zu holen oder sich einfach nur an den vielen tollen Gärten zu erfreuen.

Im Landkreis Neu-Ulm nehmen dieses Jahr folgende Gärten teil:

- Rosa und Friedrich Ziegler in Nersingen
- Petra Hauptkorn, „Kleingartenanlage Buschelbergsee“ Nersingen
- Liane und Josef Vetterl in Nersingen / Oberfahlheim
- Vereinsgarten des Gartenbauvereins in Nersingen / Oberfahlheim
- Emma und Ludwig Müller in Bellenberg
- Kreismustergarten in Weißenhorn
- Museum der Gartenkultur (Gaissmayer Illertissen)
