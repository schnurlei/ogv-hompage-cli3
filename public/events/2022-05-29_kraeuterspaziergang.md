Der Obst- und Gartenbauverein Schießen lädt alle Familien zu einem Wildkräuter-Erlebnis-Nachmittag ein.

Zusammen mit der Wildkräuterführerin Kornelia Rampp erkunden wir essbare Pflanzen in der Natur.

Zusätzlich zum Spaziergang bieten wir noch viele tolle Aktionen in der Kohlstatt in Schießen an.

Der Unkostenbeitrag pro Familie beträgt 5 €. Mitzubringen ist wetterfeste Kleidung, die auch schmutzig werden darf.

**Anmeldung bei Ilona Strobel - Tel: 0162/7584682**