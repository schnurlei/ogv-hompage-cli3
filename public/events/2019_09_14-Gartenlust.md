## »Die Liebe zur Natur«
### Großer Gartenmarkt & Aktionstag, Markttage für Freunde der Gartenkultur.

»Die Liebe zur Natur«, so lautet das Motto der 22. Illertisser Gartenlust 2019. Wir wollen damit an den großen Naturgelehrten Alexander von Humboldt erinnern, dessen Geburtstag sich am 14. September zum 250. Mal jährt.
»Die Natur muß gefühlt werden«, davon war Humboldt überzeugt. Streng wissenschaftliche Beobachtungen und Messungen allein genügten ihm nicht. Er erkannte, dass alles mit allem in Verbindung steht, wie menschliche Tätigkeit das Gleichgewicht der Natur stört. Vor über 200 Jahren warnte er bereits vor den katastrophalen Auswirkungen der Waldvernichtung auf Böden, Wasserhaushalt und Klima. Was würde er, der als Vater der Ökologie- und Umweltbewegung gilt, wohl zum heutigen Zustand unseres Planeten sagen?
Nicht Nützlichkeitsdenken und wirtschaftliche Interessen sollten im Mittelpunkt menschlichen Handelns stehen, sondern, wie Humboldt es formulierte, die »Liebe zur Natur«. Und könnten wir nicht wenigstens in unserem engsten Umfeld, in unseren Gärten, diesem Gedanken folgen?


[Link](https://www.gaissmayer.de/veranstaltungen/22-illertisser-gartenlust/)



