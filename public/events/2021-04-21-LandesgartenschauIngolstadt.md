Die Landegartenschau in Ingolstadt wurde auf das Jahr 2021 verschoben.

Die neue Laufzeit ist vom 21.4. – 3.10.2021.

Besonders interessant für ökologisch Interessierte ist das Konzept der Robinsonschen Blumenwiese, 
welches auch auf der Landesgartenschau präsentiert wird. 

[Robinsonsche Blumenwiese](https://www.gemeinnuetzige.de/projekte/oekologische-nachhaltigkeit.html)

[Bunte Blumenwiesen](https://www.lbv-muenchen.de/unsere-themen/naturnah-gaertnern/artenvielfalt-im-garten/haus-und-garten-einzelansicht/tx_ttnews/bunte-blumenwiesen.html)