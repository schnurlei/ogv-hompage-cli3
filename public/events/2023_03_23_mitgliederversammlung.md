## Mitgliederversammlung 2023

**Tagesordnung**

   1. Eröffnung und Begrüßung
   2. Grußworte der anwesenden Gäste
   3. Bericht 1. Vorsitzender
   4. Bericht Schriftführer
   5. Bericht Kassierer
   6. Entlastung Kassierer und Vorstandschaft
   7. Ehrungen


Im Anschluss an den offiziellen Teil findet wieder unsere **traditionelle Blumenverlosung** statt.

Auf Ihr zahlreiches Kommen freut sich die Vorstandschaft 