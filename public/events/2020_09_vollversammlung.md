## Mitgliederversammlung 2019

**Tagesordnung**

    1. Eröffnung und Begrüßung
    2. Bericht des 1. Vorsitzenden
    3. Bericht der Schriftführerin
    4. Bericht der Kassiererin
    5. Entlastung der Kassiererin und der Vorstandschaft
    6. Neuwahlen
    7. Ehrungen


Die Versammlung wird gemäß der aktuell gültigen Covid-19 Bestimmungen
durchgeführt. Falls sich dadurch Änderungen ergeben, werden diese
an der Anschlagtafel an der Abzweigung Stoffenrieder Str – Unteregger Str
und auf unsere Homepage www.ogv-schiessen.de bekannt gegeben.
Aufgrund der aktuellen Situation entfällt auch unsere traditionelle Blumenverlosung.
Auf Ihr zahlreiches Kommen freut sich die Vorstandschaft