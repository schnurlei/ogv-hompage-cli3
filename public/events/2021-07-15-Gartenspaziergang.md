Der OGV Schießen möchte alle interessierten Gartenbesitzer recht herzlich zu einem gemeinsamen Gang durch verschiedene Gärten im Ort einladen. 

Gartenbegehungen im Sommer bieten hervorragende Voraussetzungen, die gesamte Themenpalette des Gartens, mit allen auftretenden Fragen, am „lebendigen Objekt“ abzuhandeln.

Unter Leitung der Kreisfachberatung für Gartenkultur und Landespflege werden dabei informative Tipps zur zeitgemäßen Gartenkultur weitergegeben. Dabei zeigt sich, wie man seinen eigenen Garten in ein kleines Paradies für Mensch und Natur verwandeln kann.

Schwerpunkte der Veranstaltung:

•	Gartengestaltung
•	Sortenwahl
•	Pflanzenschutzfragen
•	Pflegearbeiten

Die Teilnahme ist kostenlos

Wir freuen uns auf Ihr Kommen!