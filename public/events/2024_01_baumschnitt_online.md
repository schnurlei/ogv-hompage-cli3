Der Obst- und Gartenbauverein Schießen lädt Sie herzlich zum Online-Kurs „Grundlagen des Obstbaumschnitt“ ein.

In dem Kurs werden die wichtigsten Grundbegriffe aus dem Bereich des Obstbaumschnitt vermittelt. Dieser Kurs vermittelt eine gute Grundlage für die folgenden Schnittkurse im Freien. Die Termine finden Sie im aktuellen Jahresprogramm des Kreisverband.

Den Link zu der Zoom Veranstaltung erhalten Sie rechtzeitig vor der Veranstaltung

**Ansprechpartner und Anmeldung:**

Rainer Schneider  
Email: streuobst@ogv­-schiessen.de

