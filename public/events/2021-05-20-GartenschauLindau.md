Neben der Landesgartenschau in Ingolstadt sind Lindau und die Region Gastgeber der 'kleinen' Bayerischen Gartenschau.

"Die Gartenschau veredelt die gesamte Stadt zu einem sinnlichen Erlebnis. Lindau wird zum blühenden Gartenstrand."
