Der Obst- und Gartenbauverein Schießen lädt alle Familien, Großeltern und interessierten Bürger zu einem Wildkräuterspaziergang ein. 

Wir starten am Sonntag den 07.04.2019 um 13:30 beim Vereinsheim in Schießen.

Zusammen mit der Allgäuer Wildkräuterführerin Karin Nothhelfer erkunden wir gemeinsam die essbaren Pflanzen in der Natur.

Zu Abschluss des Spaziergangs hauen wir die ge­sammelten Kräuter dann in die Pfanne und backen einen leckeren Kräuterpfannkuchen.

Mitzubringen ist wetterfeste Kleidung, die auch ein biss­chen schmutzig werden darf.
Der Unkostenbeitrag pro Familie beträgt 5 €

Anmeldung bei Ilona Strobel, Schleebuch.

