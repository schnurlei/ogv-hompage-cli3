## Unterallgäuer Saatgutmarkt

### »FASZINATION SAATGUT - Nur wer Vielfalt sät, wird Vielfalt ernten und Vielfalt schmecken.«

Der Kreisverband für Gartenbau und Landespflege Unterallgäu e.V.  lädt alle Gartenfreundinnen und Gartenfreunde zum 3. Unterallgäuer Saatgutmarkt am Samstag, 01.02.2025 von 10.00 Uhr bis 16.00 Uhr ein. 

Im Kirchheimer Rathaus, Marktplatz 6, warten ausgewählte Aussteller mit samenfestem und nachbaufähigem Saatgut, Infostände, ein Saatgut-Tauschtisch und interessante Vorträge auf die Besucher. 

Ergänzt wird das Marktprogramm durch den Biohof Besthans im Kirchheimer Ortsteil Derndorf, Bergstr. 1, mit weiteren Ausstellern, der an diesem Tag geöffneten „Besthans-Ladenstube“, Musik und Gesang im Klangturm und der Bewirtung mit Kaffee & Kuchen.

Der Eintritt ist frei. 


[Homepage](https://www.kirchheim-schwaben.de/veranstaltungen/2630056/2025/02/01/saatgutmarkt-in-kirchheim-i.schw..html)
