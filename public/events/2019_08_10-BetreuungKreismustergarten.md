Am 10. und 11. August übernimmt der Obst- und Gartenbauverein Schießen die Betreuung im Kreismustergarten.
 
Bei entsprechender Witterung werden wir von 14.00 Uhr bis 17.00 Uhr zur Betreuung und Beratungstätigkeit mit Rat und Tat zur Seite stehen.