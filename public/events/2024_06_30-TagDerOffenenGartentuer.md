
Der "Tag der offenen Gartentür" bietet wieder die Möglichkeit, Gärten kennen zu lernen und mit ihren Besitzerinnen ins Gespräch zu kommen.
Es ist eine schöne Gelegenheit, um sich Anregungen für den eigenen Garten zu holen oder sich einfach nur an den vielen tollen Gärten zu erfreuen.

Im Landkreis Neu-Ulm nehmen dieses Jahr folgende Gärten teil:

- Wolfgang und Karin Batke, Unterelchingen
- Bonaventura und Felicitas Mayr, Unterelchingen
- Markus und Beate Rid, Unterelchingen
- Bodo Hundsdörfer, Unterelchingen
- Kreismustergarten des Landkreises Neu-Um, Weißenhorn
