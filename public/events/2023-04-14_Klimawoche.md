## lokal handeln, global wandeln - gemeinsam aktiv für eine lebenswerte Zukunft

Nutzen Sie die Chance, sich über die diversen klimarelevanten
Themen zu informieren.

Im Rahmen der Klimawoche 2023 veranstaltet die ILE Iller-Roth-Biber am Samstag, den 22. April 2023 einen Aktionstag zum Thema Biodiversität auf dem Klostergelände in Roggenburg. Der Aktionstag hält spannende Vorträge, vielseitige Führungen und interessante Ausstellungen für die Besuchenden bereit.

[15.04.2023 Gemeinde Roggenburg-Putzete](https://roggenburg.de/detail?2023-gemeinde-roggenburg-putzete-in-roggenburg--3452)

[22.04.2023 Biodiversitätsaktionstag in Roggenburg](https://roggenburg.de/leben-wohnen/ile/biodiversitaetsaktionstag-in-roggenburg)


[Weitere Informationen](https://www.ile-iller-roth-biber.de/projekte/laufende-projekte/klimawoche-2023/)