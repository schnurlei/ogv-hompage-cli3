## Mitgliederversammlung 2022

**Tagesordnung**

    1. Eröffnung und Begrüßung
    2. Bericht des 1. Vorsitzenden
    3. Bericht der Schriftführerin
    4. Bericht der Kassiererin
    5. Nachwahl Kassenprüfer
    6. Entlastung der Kassiererin und der Vorstandschaft
    7. Ehrungen


Im Anschluss an den offiziellen Teil findet wieder unsere **traditionelle Blumenverlosung** statt.

Auf Ihr zahlreiches Kommen freut sich die Vorstandschaft