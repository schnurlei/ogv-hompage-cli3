## Illertisser Saatgutmarkt 

### »Vielfalt sehen, säen und erhalten«


Aussteller, Vorträge und Aktionen rund um die Kultur- und Wildpflanzenvielfalt

[Homepage](https://www.gaissmayer.de/web/gaertnerei/veranstaltungen/vorort/illertisser-saatgutmarkt-2022/)
