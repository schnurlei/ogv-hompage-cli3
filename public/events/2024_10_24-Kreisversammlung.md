Die Mitgliederversammlung des Kreisverbandes für Gartenbau und Landespflege findet
dieses Jahr in Schießen statt.

**Datum:** Donnerstag, den 24.10.2024, um 19.00 Uhr

**Ort:**  Vereinsheim Schießen, Stoffenrieder Str. 2 in 89297 Roggenburg/Schießen.

## Tagesordnung

1. Begrüßung durch den 1. Vorsitzenden
2. Grußworte
3. Rechenschaftsbericht des 1. Vorsitzenden
4. Kassenbericht - Bericht der Kassenprüfer
5. Entlastung der Vorstandschaft
6. „Faszination Wildbienen - Artenvielfalt im Landkreis Neu-Ulm“
   Vortrag von Herrn Jonas Benner – Biodiversitätsberater vom Landratsamt Neu-Ulm
7. Ehrungen und Übergabe der Diplome „Naturgarten – Bayern blüht“ - „Wettbewerbe 2024“

Musikalisch umrahmt wird die Veranstaltung vom Kirchenchor Biberach.
Zu dieser Versammlung sind alle Vereinsmitglieder der Gartenbauvereine eingeladen.