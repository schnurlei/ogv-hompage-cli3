Der Obst- und Gartenbauverein Schießen möchte alle interessierten Gartenbesitzer recht herzlich zu einem Obstgehölz-Pflegekurs einladen. Ein besonderes Augenmerk legen wir bei diesem Kurs auf den Erziehungsschnitt junger Bäume.

Unsere Schnittmaßnahmen an Gehölzen nehmen erheblichen Einfluss auf:

* Gesundheit
* Blühwilligkeit
* Lebensalter
* Langlebigkeit
* Fruchtbarkeit
* Arbeitsaufwand


Unter der Leitung von Herrn Rudolf Siehler, Kreisfachberater für Gartenkultur und Landespflege vom Landratsamt Neu-Ulm, wird den Teil­nehmern der rich­tige Schnitt eines Obstbaumes praxisnah vor Augen geführt. 

Die Teilnahme ist kostenlos, eine Baumschere sollte mitgebracht werden.

Der Obst- und Gartenbauverein Schießen freut sich auf Ihr Kommen !

**Ansprechpartner und Anmeldung:**

Rainer Schneider, Obst- und Gartenbauverein Schießen, erster.vorsitzender@ogv-schiessen.de