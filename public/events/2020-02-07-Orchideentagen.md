Vom 7. bis zum 9. Februar 2020 ist das Neu-Ulmer Edwin-Scharff-Haus zum 20. Mal Gastgeber einer international anerkannten Blumenschau.

Zahlreiche regionale, nationale und internationale Orchideenzüchter präsentieren im Neu-Ulmer Edwin-Scharff-Haus ihre schönsten Exemplare.
 
