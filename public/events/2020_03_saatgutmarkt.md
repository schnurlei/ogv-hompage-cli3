## Illertisser Saatgutmarkt 

### Vielfalt sehen, säen und erhalten

Vorträge, Aussteller und Aktionen rund um die Saatgutvielfalt unserer Kulturpflanzen. 

[Homepage](https://www.gaissmayer.de/web/gaertnerei/veranstaltungen/vorort/illertisser-saatgutmarkt-2020/)
