## Bayerischen Landesgartenschau 2024 in Kirchheim bei München

### Zusammen.Wachsen.

Unter dem Motto Zusammen.Wachsen. laden wir Sie 2024 in den neuen 10 ha großen Ortspark zwischen Kirchheim und Heimstetten ein, den Sie zum Entdecken, Spielen und Erholen nutzen können.