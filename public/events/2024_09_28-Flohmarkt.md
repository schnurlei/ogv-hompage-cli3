Der Obst- und Gartenbauverein Schießen veranstaltet am 28.09.2024 einen Familien- und Gartenflohmarkt mit
Pflanzentauschbörse.
Verkauft werden dürfen Artikel wie: Spielsachen, Kleidung, Bücher, Pflanzenableger, alte Gartengeräte, ...

##### Stände dürfen nur von privaten Anbietern angemeldet werden
Anzahl der Stände begrenzt -
Vergabe in Anmeldereihenfolge

#####  Die Standgebühr beträgt 5 €
max. 3,0 x 0,6 m, Standard-Tapeziertisch

##### Verbindliche Anmeldungen für Verkaufsstände  unter:

**E-Mail:** flohmarkt@ogv-schiessen.de

**Telefon:** 0178/ 683 9140





