Die bundesweite Vogel-Zählaktion findet jedes Jahr am ersten  Januarwochenende  statt.  Alle  Naturfreunde  sind dazu  aufgerufen,  Vögel  zu  beobachten  und  zu  melden.  

Ziel der Aktion ist es, ein deutschlandweites und möglichst genaues  Bild  von  der  Vogelwelt  in  unseren  Städten  und Dörfern zu erhalten. Dabei geht es nicht um eine vollständige Erfassung aller Vögel, sondern darum, Veränderungen der Vogelbestände festzustellen.

 www.stundederwintervoegel.de

Bild: Jacob  Spinks from Northamptonshire, England 
[Creative Commons Attribution 2.0 Generic License](https://creativecommons.org/licenses/by/2.0)

