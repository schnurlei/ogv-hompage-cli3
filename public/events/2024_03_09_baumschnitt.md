Der Obst- und Gartenbauverein Schießen möchte alle interessierten Gartenbesitzer recht herzlich zu einem Obstgehölz-Pflegekurs einladen. Ein besonderes Augenmerk legen wir bei diesem Kurs auf den Erziehungsschnitt junger Bäume.

Unsere Schnittmaßnahmen an Gehölzen nehmen erheblichen Einfluss auf:

* Gesundheit
* Blühwilligkeit
* Lebensalter
* Langlebigkeit
* Fruchtbarkeit
* Arbeitsaufwand


Die Teilnahme ist kostenlos, eine Baumschere sollte mitgebracht werden.

Der Obst- und Gartenbauverein Schießen freut sich auf Ihr Kommen !

**Ansprechpartner und Anmeldung:**

Rainer Schneider, Obst- und Gartenbauverein Schießen, erster.vorsitzender@ogv-schiessen.de