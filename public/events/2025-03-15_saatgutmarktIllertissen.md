## Illertisser Saatgutmarkt

### »Vielfalt sehen, säen und erhalten«


Aussteller, Vorträge und Aktionen rund um die Kultur- und Wildpflanzenvielfalt

### VORTRÄGE DER VIELFALT in der Gartenbibliothek

|    Uhr |         |  |
|-------:| ----------:| ---------------- | 
|  11.00 |  Saatgut aus dem Hausgarten | Lucia Hiemer, Keimgut- und Saatenkooperative Allgäu|
|  12.00 | Knoblauch als alte Kultur- und Heilpflanze | Bernd Socher, Landschaftschmecken |
|  13.00 | Veredlungstechnik an Obstbäumen | Franz Rendle, Förderer der Gartenkultur e.V.|
|  14.00 | Gärtnern mit dem Regenwurm | Ingo Hubl, Vielfaltsgärtner|

[Homepage](https://www.gaissmayer.de/web/gaertnerei/veranstaltungen/vorort/illertisser-saatgutmarkt-2025/)
