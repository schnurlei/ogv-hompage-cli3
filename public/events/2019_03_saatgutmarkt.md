## Illertisser Saatgutmarkt 

Vorträge, Aussteller und Aktionen rund um die Saatgutvielfalt unserer Kulturpflanzen. 
In Kombination mit “Frühling in den Stauden” der benachbarten Staudengärtnerei Gaißmayer

[Homepage](https://www.gaissmayer.de/veranstaltungen/illertisser-saatgutmarkt-2019/)