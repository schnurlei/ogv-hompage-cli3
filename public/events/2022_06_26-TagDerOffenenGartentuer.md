Nachdem der Tag der offenen Gartentür 2020 und 2021 aufgrund der Corona-Pandemie leider entfallen ist,
wird 2022 ein neuer Anlauf gestartet.

Der "Tag der offenen Gartentür" bietet wieder die Möglichkeit, Gärten kennen zu lernen und mit ihren Besitzerinnen ins Gespräch zu kommen.
Es ist eine schöne Gelegenheit, um sich Anregungen für den eigenen Garten zu holen oder sich einfach nur an den vielen tollen Gärten zu erfreuen.

Im Landkreis Neu-Ulm nehmen dieses Jahr folgende Gärten teil:

- Kreismustergarten, Ulmer Str.31, 89264 Weißenhorn
- Kleingartenanlage Tiefenbach „Am Seelach“ 89257 Illertissen OT Tiefenbach
- Kerstin Meyer, Begonienweg 28, 89297 Roggenburg-Schießen
- Rainer Schneider, Kirchplatz 8, 89297 Roggenburg-Schießen