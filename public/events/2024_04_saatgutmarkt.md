## Illertisser Saatgutmarkt

### »Vielfalt sehen, säen und erhalten«


Aussteller, Vorträge und Aktionen rund um die Kultur- und Wildpflanzenvielfalt

Für das leibliche Wohl ist gesorgt.

**VORTRÄGE DER VIELFALT im Glashaus**

- 11.00 Uhr N.N. I Patrick Kaiser, Genbänkle
- 12.00 Uhr Die tolle Knolle I Christian Müller, Raritätenhof Müller
- 13.00 Uhr N.N
- 14.00 Uhr Tomatenvielfalt I Michael Schick, Naturerlebnis Michi Schick

[Homepage](https://www.gaissmayer.de/web/gaertnerei/veranstaltungen/vorort/illertisser-saatgutmarkt-2024/)