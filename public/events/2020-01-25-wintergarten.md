## Gärten im Winter - Ein Gartenspaziergang

Eigentlich sollte man meinen, der Winter sei ein denkbar schlechter Zeit­punkt für einen Gartenspaziergang.
Aber auch im Winter gibt es im Garten vieles zu entdecken: 

    • Gehölze mit attraktiver Rinde
    • winterblühende Stauden
    • Samenstände von Gräsern und vieles mehr

Der Gartenspaziergang bietet die Ge­le­gen­heit, interessante Gehölze, Stauden und Gräser für den ‚Wintergarten‘ kennen zu ler­nen und sich über eigene Er­fahrungen auszutauschen

**Treffpunkt:** 	
Kirchplatz 8, Roggenburg/Schießen (neben Kirche)
